parasails.registerPage('available-things', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    showDeleteThingModal: false,
    thingIdToBeDeleted: null,
    syncing: false,
    cloudError: "",
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    _.extend(this, SAILS_LOCALS);
    this.things = this.things.map(item => {
      return {
        ...item,
        showDelete: false
      };
    });
  },
  mounted: async function() {
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    showDeleteModal: function (thingId) {
      this.thingIdToBeDeleted = thingId;
      this.showDeleteThingModal = true;
    },
    hideDeleteModal() {
      this.thingIdToBeDeleted = null;
      this.showDeleteThingModal = false;
    },
    handleParsingDeleteThingForm() {
      return {
        id: this.thingIdToBeDeleted
      };
    },
    submittedDeleteThingForm() {
      _.remove(this.things, { id: this.thingIdToBeDeleted });
      this.$forceUpdate();
      this.hideDeleteModal();
    }
  }
});
